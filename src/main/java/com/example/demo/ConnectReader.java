package com.example.demo;
import javax.smartcardio.*;

public class ConnectReader {

    /**
     * @param index of the terminal list
     * @throws CardException if there are problems accessing the smartcard
     */
    public String connectCard(int index) throws CardException {

        //case if no reader is connected
        if (TerminalFactory.getDefault().terminals().list().size() == 0) {
            return "No reader present";
        }

        //get the actual terminal
        CardTerminal terminal = TerminalFactory.getDefault().terminals().list().get(index);

        //Case if no card is present
        if (!terminal.isCardPresent()) {
            return "No Card present!";
        }

        //Connect to the card with the right terminal
        Card card = terminal.connect("T=1");


        //Reset the card for use by using ATR
        ATR atr = card.getATR();


        // Get the basic channel.
        CardChannel channel = card.getBasicChannel();

        //Get UID by using the certain command
        byte[] command = {(byte) 0xFF, (byte) 0xCA, (byte) 0x00, (byte) 0x00, (byte) 0x00};


        CommandAPDU someApdu = new CommandAPDU(command);

        //transmit the Command to the card
        ResponseAPDU r = channel.transmit(someApdu);
        // get ResponseAPDU in bytes
        byte[] response = r.getBytes();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < response.length; i++) {
            byte b = response[i];
            //replace the bytes with Hex-format
            builder.append(String.format("%02x", b));
        }
        card.disconnect(false);
        //return the UID in Hex-format
        return builder.toString().toUpperCase();

    }

    public String getUID(int index) throws CardException {
        CardTerminal terminal = TerminalFactory.getDefault().terminals().list().get(index);
        while (true) {
            //only returns something if any card is present
            if (terminal.isCardPresent()) {
                try {
                    return connectCard(index);
                } catch (Exception e) {
                }
            }
        }
    }
}

