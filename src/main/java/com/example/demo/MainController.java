package com.example.demo;

import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Objects;


public class MainController {
    public  TextField statusField;
    public TableView<Person> tableView;
    public Button showHelpers;
    public Button showHelpersNow;
    public CheckBox showFirst;
    public CheckBox showFire;
    public Button deleteHelpers;
    @FXML
    private GridPane rootPane;
    public TableColumn<Person,String> id;
    public TableColumn<Person,String> name;
    public TableColumn<Person,String> firstName;
    public TableColumn<Person,String> tel;
    public TableColumn<Person,Integer> firstHelper;
    public TableColumn<Person,Integer> fireHelper;

    public Button signUp;
    @FXML
    private Label welcomeText;
    private final Backend backend = new Backend();
    private final ActionEvent actionEvent = new ActionEvent();
    private ObservableList<Person> dbData = null;

    /**
     * initialises table view by start of program
     * starts task for card reader
     * shows helpers at the moment in the table
     * @throws SQLException
     */
    public void initialize() throws SQLException {
        updateStatus(statusField);
        id.setCellValueFactory(new PropertyValueFactory<Person,String>("ID"));
        name.setCellValueFactory(new PropertyValueFactory<Person,String>("name"));
        firstName.setCellValueFactory(new PropertyValueFactory<Person,String>("firstName"));
        tel.setCellValueFactory(new PropertyValueFactory<Person,String>("tel"));
        firstHelper.setCellValueFactory(new PropertyValueFactory<Person,Integer>("firstHelper"));
        fireHelper.setCellValueFactory(new PropertyValueFactory<Person,Integer>("fireHelper"));
        this.showHelpersNow(actionEvent);
    }

    /**
     * opens next window to sign up by clicking on sign up button
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void onSignUpButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("signupform-view.fxml")));
        Stage stage = new Stage();
        Scene scene = new Scene(root, 500, 600);
        stage.setTitle("Registrieren");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * delays running of task
     * @param millis
     * @param continuation
     */
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }

    /**
     * background task to update status of current users
     * @param statusField
     */
    public void updateStatus(TextField statusField){
        Backend backend = new Backend();
        Task<Void> task = new Task<>() {
            private Object showHelpersNow;
            @Override
            public Void call() throws Exception {
                ConnectReader reader = new ConnectReader();
                CardTerminal terminal = TerminalFactory.getDefault().terminals().list().get(0);
                while (true) {
                    if (terminal.isCardPresent()) {
                        statusField.setText(backend.updateStatus(reader.getUID(0)));
                        showHelpersNow(actionEvent);
                        delay(3000, () -> statusField.setText(""));
                        while (terminal.isCardPresent()) {
                            Thread.sleep(500);
                        }
                    }
                    if (LocalTime.now().equals(LocalTime.MIDNIGHT)){
                        backend.resetStatus();
                    }
                }
            }
        };
        new Thread(task).start();
    }

    /**
     * shows helpers in table
     * filters with checkbox
     * @param actionEvent
     * @throws SQLException
     */
    public void showAllHelpers(ActionEvent actionEvent) throws SQLException {
        if (showFirst.isSelected()&&!showFire.isSelected()) {
            dbData = backend.getHelpers(0);
        }
        if (!showFirst.isSelected()&&showFire.isSelected()) {
            dbData = backend.getHelpers(1);
        }
        if (showFirst.isSelected()&&showFire.isSelected()) {
            dbData = backend.getHelpers(2);
        }
        if (!showFirst.isSelected() && !showFire.isSelected()) {
            dbData = backend.getHelpers(3);
        }
        tableView.setItems(dbData);
        deleteHelpers.setVisible(true);
    }

    /**
     * shows helpers at the place in table
     * filters by checkbox
     * @param actionEvent
     * @throws SQLException
     */
    public void showHelpersNow(ActionEvent actionEvent) throws SQLException {
        if (showFirst.isSelected()&&!showFire.isSelected()) {
            dbData = backend.getHelpers(4);
        }
        if (!showFirst.isSelected()&&showFire.isSelected()) {
            dbData = backend.getHelpers(5);
        }
        if (showFirst.isSelected()&&showFire.isSelected()) {
            dbData = backend.getHelpers(6);
        }
        if (!showFirst.isSelected() && !showFire.isSelected()) {
            dbData = backend.getHelpers(7);
        }
        tableView.setItems(dbData);
        deleteHelpers.setVisible(false);
    }

    /**
     * deletes helper from table
     * available only from helpers view
     * @param actionEvent
     */
    public void onDeleteHelpersClick(ActionEvent actionEvent) {
        int selectedID = tableView.getSelectionModel().getSelectedIndex();
        String selectedUID = dbData.get(tableView.getSelectionModel().getSelectedIndex()).getID();
        tableView.getItems().remove(selectedID);
        backend.deleteHelper(selectedUID);
    }
}
