package com.example.demo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;

/**
 * methods for sql commands
 */
public class Backend {
    private Statement stmt;
    private final String[] selects = new String[8];

    public Backend() {
        try {
            // conection with database
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/presenthelpers", "root", "");
            this.stmt = con.createStatement();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    /**
     * Creates an entry in the database for the person
     *
     * @param person Person to sign up
     */
    public void signUp(Person person) {
        try {
            // Create entry for person
            stmt.executeUpdate(String.format(
                    "INSERT INTO helpers VALUES ('%s', '%s', '%s','%s', %d, %d, 0)",
                    person.getID(), person.getName(), person.getFirstName(), person.getTel(), person.getFirstHelper(), person.getFireHelper()));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Returns list of <code>Person</code> objects created with results from the query
     *
     * @param n index for certain select query
     * @return Result of query
     */
    public ObservableList<Person> getHelpers(int n) throws SQLException {
        selects[0] = "SELECT * FROM helpers WHERE firstHelper = 1;";
        selects[1] = "SELECT * FROM helpers WHERE fireHelper = 1;";
        selects[2] = "SELECT * FROM helpers WHERE firstHelper = 1 AND fireHelper = 1;";
        selects[3] = "SELECT * FROM helpers;";
        selects[4] = "SELECT * FROM helpers WHERE firstHelper = 1 AND status = 1;";
        selects[5] = "SELECT * FROM helpers WHERE fireHelper = 1 AND status = 1;";
        selects[6] = "SELECT * FROM helpers WHERE firstHelper = 1 AND fireHelper = 1 AND status = 1;";
        selects[7] = "SELECT * FROM helpers WHERE status = 1;";

        ResultSet resultSet;
        try {
            // execute chosen query
            resultSet = stmt.executeQuery(selects[n]);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ArrayList<Person> data = new ArrayList<>();
        while (resultSet.next()) {
            Person person = new Person(resultSet.getString("id"), resultSet.getString("name"), resultSet.getString("firstName"), resultSet.getString("tel"), resultSet.getInt("firstHelper"), resultSet.getInt("fireHelper"));
            data.add(person);
        }
        return FXCollections.observableArrayList(data);
    }

    /**
     * Update the presence of the particular person according to uid
     *
     * @param uid ID of the person
     * @return Response message
     */
    public String updateStatus(String uid) {
        String result = new String();
        if (isHelper(uid)) {
            try {
                ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM helpers WHERE ID = '%s';",uid));
                Person person = null;
                int st = 2;
                while(rs.next()){
                    person = new Person(rs.getString("id"), rs.getString("name"), rs.getString("firstName"), rs.getString("tel"), rs.getInt("firstHelper"), rs.getInt("fireHelper"));
                    st = rs.getInt("status");
                }
                if (st==0){
                    stmt.executeUpdate(String.format("UPDATE helpers SET status=1 WHERE ID = '%s';",uid));
                    return result = person.getFirstName()+" "+person.getName()+ " ist angemeldet";
                } else if(st==1){
                    stmt.executeUpdate(String.format("UPDATE helpers SET status=0 WHERE ID = '%s';",uid));
                    return result = person.getFirstName()+" "+person.getName()+ " ist abgemeldet";
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } return "Person ist kein Helfer";
    }

    /**
     * set status to 0 for the next day
     */
    public void resetStatus(){
        try {
            stmt.executeUpdate("UPDATE helpers SET status=0;");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * checks if user is a helper
     * @param id card id
     * @return true/false
     */
    public boolean isHelper(String id) {
        try {
            // look for entry with id
            ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM helpers WHERE ID = '%s';", id));

            // if the result has an entry, person of id is a helper
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    /**
     * deletes helper from table
     * @param uid card id
     */
    public void deleteHelper(String uid){
        try {
            stmt.executeUpdate(String.format("DELETE FROM helpers WHERE ID = '%s';",uid));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
