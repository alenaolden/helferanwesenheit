package com.example.demo;
import java.util.List;
import javax.smartcardio.*;

public class ListSmartCardReaders {
    public static int listCounted() {
        //get the Default TerminalFactory
        TerminalFactory factory = TerminalFactory.getDefault();
        try {
            //List of all possible Terminals on this System
            List<CardTerminal> terminals = factory.terminals().list();
            for (CardTerminal terminal : terminals) {
                System.out.println("Card Terminal: "+ terminal.getName());
                System.out.println("Card present?: "+terminal.isCardPresent());
                System.out.println("————————————–");
            }
            return terminals.size();

        } catch (CardException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
        listCounted();
    }
}