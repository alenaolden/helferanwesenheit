package com.example.demo;

/**
 * to save data of helpers from database
 */
public class Person {
    private final String ID;
    private final String name;
    private final String firstName;
    private final String tel;
    private final int firstHelper;
    private final int fireHelper;

    public Person(String ID, String firstName, String name, String tel, int firstHelper, int fireHelper){
        this.ID = ID;
        this.name = name;
        this.firstName = firstName;
        this.tel = tel;
        this.firstHelper = firstHelper;
        this.fireHelper = fireHelper;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getName() {
        return name;
    }

    public String getID() {
        return ID;
    }

    public String getTel() {
        return tel;
    }

    public int getFirstHelper(){
        return firstHelper;
    }

    public int getFireHelper(){
        return fireHelper;
    }

}
