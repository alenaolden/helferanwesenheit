package com.example.demo;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.smartcardio.CardException;

public class SignUpFormController {

    public Label name;
    public Label firstName;
    public TextField nameField;
    public TextField firstNameField;
    public CheckBox checkFirst;
    public CheckBox checkFire;
    public Button signUp;
    public Label telNumber;
    public TextField telNumberField;
    private String uid;

    /**
     * reads card id by window opening
     * @throws CardException
     */
    public void initialize() throws CardException {
        ConnectReader reader=new ConnectReader();
        uid = reader.getUID(0);
    }

    /**
     * adds new user by clicking registration button
     * @param actionEvent
     */
    public void addNewUser(ActionEvent actionEvent) {
        int flag1 = 0;
        int flag2 = 0;
        if(checkFirst.isSelected()){
            flag1 = 1;
        }
        if (checkFire.isSelected()){
            flag2 = 1;
        }
        Person person = new Person(uid, nameField.getText(), firstNameField.getText(), telNumberField.getText(), flag1, flag2);
        Backend backend = new Backend();
        backend.signUp(person);
        ((Stage)(((Button)actionEvent.getSource()).getScene().getWindow())).close();
    }
}
