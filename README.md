# 1. Helferanwesenheit
## 1.1. Description

***
>Ziel dieses Projektes ist es, das Ein- und Austragen von Erst- und Brandhelfern zu automatisieren. Dabei sollen Kenntnise mit grafischer Programmierung und der Umgang mit Datenbanken gewonnen werden.

***

## 1.2. Table of Content

****

- [1. Helferanwesenheit](#1-helferanwesenheit)
  - [1.1. Description](#11-description)
  - [1.2. Table of Content](#12-table-of-content)
  - [1.3. Installation Guide](#13-installation-guide)
  - [1.4. General Structure](#14-general-structure)
    - [1.4.1. Database Structure](#141-database-structure)
  - [1.5. Classes](#15-classes)
    - [public class Backend()](#public-class-backend)
    - [public class CardApplication()](#public-class-cardapplication)
    - [public class ConnectReader()](#public-class-connectreader)
    - [public class ListSmartCardReaders()](#public-class-listsmartcardreaders)
    - [public class MainController()](#public-class-maincontroller)
    - [public class Person()](#public-class-person)
    - [public class SignUpFormController()](#public-class-signupformcontroller)
  - [1.6. Future Ideas](#16-future-ideas)
  - [1.7. Teams](#17-teams)
    - [1.7.1. Team Backend](#171-team-backend)
      - [1.7.2.1. Mitglieder](#1721-mitglieder)
      - [1.7.2.2. Aufgabe](#1722-aufgabe)
      - [1.7.2.3. Vorgehen](#1723-vorgehen)
      - [1.7.3.4. Schwierigkeiten](#1734-schwierigkeiten)
    - [1.7.2. Team GUI](#172-team-gui)
      - [1.7.2.1. Mitglieder](#1721-mitglieder-1)
      - [1.7.2.2. Aufgabe](#1722-aufgabe-1)
      - [1.7.2.3. Vorgehen](#1723-vorgehen-1)
      - [1.7.2.4. Schwierigkeiten](#1724-schwierigkeiten)
    - [1.7.3. Team Kartenleser](#173-team-kartenleser)
      - [1.7.3.1. Mitglieder](#1731-mitglieder)
      - [1.7.3.2. Aufgabe](#1732-aufgabe)
      - [1.7.3.3. Vorgehen](#1733-vorgehen)
      - [1.7.3.4. Schwierigkeiten](#1734-schwierigkeiten-1)
    - [1.7.4. Verbindung der Datenbank und des Kartenlesers mit GUI](#173-team-verbindung)
      - [1.7.4.1. Vorgehen](#1731-vorgehen)
      - [1.7.4.2. Schwierigkeiten](#1732-schwierigkeiten)

## 1.3. Installation Guide

****
Für das Programm wird benötigt:
 - JDK 17.0.5
 - Webserver Apache2
 - Maria DB
 - Maven version 3.8.6
 - JavaFX 19

## 1.4. General Structure

****
Daten per Kartenleser und Benutzereingabe in GUI einlesen, in einer Datenbank speichern und diese auf Abfrage wieder ausgeben.

### 1.4.1. Database Structure

****
Datenbank "presenthelpers" , bestehend aus einer Tabelle "helpers" mit den Attributen ID, Name, Vorname, Telefonnummer, Ersthelfer, Brandschutzhelfer und Anwesenheitsstatus.

![Tabelle "helpers" Struktur](https://gitlab.com/alenaolden/helferanwesenheit/blob/main/helpers_table.PNG?raw=true)

Zum Einloggen wird mariadb Benutzer mit root Berechtigungen benötigt.

## 1.5. Classes

### public class Backend()

**** 
Klasse für Funktionalität von SQL: Verbindung mit Datenbank, Select, Update, Delete
### public class CardApplication()

**** 
Main: Start des Programms
### public class ConnectReader()

**** 
Verbindung mit Kartenleser und Auslesen der Card ID
Eine Anleitung wie man den My SQLConnector in das Java Programm lädt, gibt es hier:
https://falconbyte.net/java-mysql.php#2
Downloaden kann man ihn hier:
https://dev.mysql.com/downloads/connector/j/

### public class ListSmartCardReaders()

**** 
Ausgabe von verbundenen Lesegeräten
### public class MainController()

**** 
Funktionalität des GUI
### public class Person()

**** 
Klasse zum Speichern der Helfer
### public class SignUpFormController()

**** 
Nutzerregistrierung in GUI
## 1.6. Future Ideas

****
Übertragung Programm auf Raspberry Pi
## 1.7. Teams

****
Böngler Erik, Endreß Hannah, König Leon, Olderburger Alena, Pieper Yannick, Rosin Jonas, Wrede Cilia, Zeineddine Zulfikar

### 1.7.1. Team Backend

#### 1.7.1.1. Mitglieder
****
Böngler Erik, Endreß Hannah, Rosin Jonas
#### 1.7.1.2. Aufgabe
****
Programmieren einer Schnittstelle für den Zugriff der GUI und des Kartenlesers auf eine lokale MariaDB Datenbank.

#### 1.7.1.3. Vorgehen

****
Erstellen der Klasse Person, um den Datenaustausch zu vereinfachen. 
Aufsetzen der Datenbank und Verbindung mit der Klasse Backend.
Implementierung der Funktionen der Klasse Backend und Testung dieser. 
Programmierung temporärer Test für die Methoden.  

#### 1.7.1.4. Schwierigkeiten
****
Schwierigkeiten hatten wir unter anderem bei der Verknüpfung von Formated-Strings und dem Verbinden der Datenbank mit dem Code. Erstellen von Test um die Methoden zu testen. Überwunden haben wir die meisten Schwierigkeiten durch eine gründliche Recherche und dem nachvollziehen der Methoden. Die Verküpfung mit der Datenbank haben wir mit der Implementation der J-Connector-Klasse gelöst. 

### 1.7.2. Team GUI / Framework JavaFX

#### 1.7.2.1. Mitglieder
****
Oldenburger Alena, Wrede Cilia, Zeineddine Zulfikar

#### 1.7.2.2. Aufgabe
****
Erstellung der Benutzeroberfläche und ihrer Funktionalität für das Programm.

#### 1.7.2.3. Vorgehen
****
GUI designen und erstellen.
Einsicht Datenbank für Admins.
Registrierung für neue Helfer. 
Löschung eingetragener Helfer.
Implementierung der Funktionen für Buttons und Checkboxes.
#### 1.7.2.4. Schwierigkeiten
****
FXML Syntax, Implementierung eines Frameworks in Java, Vorheriges Ausarbeiten der konkreten Struktur der GUI nötig
### 1.7.3. Team Kartenleser

#### 1.7.3.1. Mitglieder
****
König Leon, Pieper Yannick

#### 1.7.3.2. Aufgabe
****
Herstellung der Verbindung zum Kartenleser und Methodik zum Auslesen der Card ID.

#### 1.7.3.3. Vorgehen
****
Verwendung eines kontaktlosen Scanners, welcher die Abrufung der ID einer Smartcard mit javax.smartcardio ermöglicht.  
Verwendung der API von smartcardio mit dessen Klassen und Methoden.
Recherche über die byte-Befehle in der Dokumentation des Scanners.  
Ausgabe der ID in Hexadarstellung in Form eines Strings. 

#### 1.7.3.4. Schwierigkeiten
****
Anfangs Probe mit Kontaktkartenleser in JAVA, die ID einer Karte abzurufen.
Feststellung, dass dies mit smartcardio bei Kontakt-Scannern nicht möglich ist.
### 1.7.4. Verbindung der Datenbank und des Kartenlesers mit GUI

#### 1.7.4.1. Vorgehen
****
Verbindung Programm mit Datenbank.
Verbindung mit Methodik für Kartenleser.  
Multithreading zur gleichzeitigen Karteneinlese und Datenbankbearbeitung.
Anpassen GUI an Methoden.

#### 1.7.4.2. Schwierigkeiten
****
Wenig Absprache bezüglich der Datenbankstruktur, daher anpassen der Methoden nötig.
Version ohne Multithreading: nur über Button möglich, den Kartenleser neu abzufragen.
Probleme mit Dokumentation.
